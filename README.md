# Front End Platform - [![CircleCI](https://circleci.com/bb/abalmus/front-end-platform.svg?style=svg)](https://circleci.com/bb/abalmus/front-end-platform)

# Documentation
- [Validation Processor](/packages/validator/README.md)
- [React Form with validation](/packages/react/src/components/Form/README.md)